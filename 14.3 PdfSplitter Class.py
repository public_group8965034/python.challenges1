from PyPDF2 import PdfReader, PdfWriter

from pathlib import Path

class PdfSplitter:
    def __init__(self, path):
        self.pdf_path = PdfReader(path)
        self.writer1 = PdfWriter()
        self.writer2 = PdfWriter()

    def split(self, page_numb):
        for page in self.pdf_path.pages[0:page_numb]:
            self.writer1.add_page(page)

        for page in self.pdf_path.pages[page_numb:-1]:
            self.writer2.add_page(page)

    def write(self, filename):
        with Path(filename +"_1.pdf").open(mode="wb") as output_file:
            self.writer1.write(output_file)

        with Path(filename +"_2.pdf").open(mode="wb") as output_file:
            self.writer2.write(output_file)

pdf_file = (
    Path.home() /
    "Documents" /
    "practice_files" /
    "Pride_and_Prejudice.pdf"
)

pdf_splitter = PdfSplitter(str(pdf_file))
pdf_splitter.split(150)
pdf_splitter.write("new_file")
