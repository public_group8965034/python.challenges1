chessboard = [
    ['0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H','0'],
    ['1', 'o', '.', '.', '.', 'o', '.', 'o', '.','1'],
    ['2', '.', 'x', '.', 'x', '.', '.', '.', 'o','2'],
    ['3', 'x', '.', '.', '.', '.', '.', 'x', '.','3'],
    ['4', '.', '.', '.', 'x', '.', '.', '.', '.','4'],
    ['5', 'o', '.', '.', '.', '.', '.', '.', '.','5'],
    ['6', '.', 'o', '.', 'x', '.', 'x', '.', 'o','6'],
    ['7', 'o', '.', 'x', '.', 'x', '.', '.', 'o','7'],
    ['8', '.', '.', '.', '.', '.', 'x', '.', '.','8'],
    ['0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H','0']
]



# zdefiniować klase Pionek i dać atrybut ruchy na przykład
class PawnX:
    def __init__(self, pawn_position):
        self.i, self.j = pawn_position

    def current_position(self):
        position = (self.i, self.j)
        return position

    def possible_moves_x(self, chessboard):

        moves_x = []  # lista na możliwe ruchy
        death_pawns_x = []


        # Jeśli mozliwe jest przesunięcie w lewo i to miejsce jest wolne
        if self.j > 1 and chessboard[self.i - 1][self.j - 1] == '.':
            moves_x.append((self.i - 1, self.j - 1))
        # Jeśli możliwe jest przesunięcie w prawo i to miejsce jest wolne
        if self.j < 8 and chessboard[self.i - 1][self.j + 1] == '.':
            moves_x.append((self.i - 1, self.j + 1))

        # Dodaj sprawdzenia dla bicia
        # Tutaj zabezpieczamy się przed wyjściem poza zakres planszy
        if self.i > 1 and self.j > 1 and (chessboard[self.i - 1][self.j - 1].lower() == 'o') and chessboard[self.i - 2][
            self.j - 2] == '.':
            moves_x.append((self.i - 2, self.j - 2))
            death_pawns_x.append((self.i - 1, self.j - 1))

        if self.i > 1 and self.j < 7 and (chessboard[self.i - 1][self.j + 1].lower() == 'o') and chessboard[self.i - 2][
            self.j + 2] == '.':
            moves_x.append((self.i - 2, self.j + 2))
            death_pawns_x.append((self.i - 1, self.j + 1))

        return moves_x, death_pawns_x

class PawnO:
    def __init__(self, pawn_position):
        self.i, self.j = pawn_position

    def current_position(self):
        position = (self.i, self.j)
        return position

    def possible_moves_o(self, chessboard):

        moves_o = []  # lista na możliwe ruchy
        death_pawns_o = []


        # Jeśli mozliwe jest przesunięcie w lewo i to miejsce jest wolne
        if self.j > 1 and chessboard[self.i + 1][self.j - 1] == '.':
            moves_o.append((self.i + 1, self.j - 1))
        # Jeśli możliwe jest przesunięcie w prawo i to miejsce jest wolne
        if self.j < 8 and chessboard[self.i + 1][self.j + 1] == '.':
            moves_o.append((self.i + 1, self.j + 1))

        # Dodaj sprawdzenia dla bicia
        # Tutaj zabezpieczamy się przed wyjściem poza zakres planszy
        if self.i < 7 and self.j < 7 and (chessboard[self.i + 1][self.j + 1].lower() == 'x') and chessboard[self.i + 2][
            self.j + 2] == '.':
            moves_o.append((self.i + 2, self.j + 2))
            death_pawns_o.append((self.i + 1, self.j + 1))

        if self.i > 0 and self.j < 7 and (chessboard[self.i + 1][self.j - 1].lower() == 'x') and chessboard[self.i + 2][
            self.j - 2] == '.':
            moves_o.append((self.i + 2, self.j - 2))
            death_pawns_o.append((self.i + 1, self.j - 1))

        return moves_o, death_pawns_o


def print_chessboard():
    for line in chessboard:
        print(' '.join(line))

def check_for_pieces(piece):
    return any(piece in row or piece.upper() in row for row in chessboard)



x_found = check_for_pieces('x') #sprawdza, czy są małe bądzi duże X na planszy
o_found = check_for_pieces('o')


def convert_input_coordinates(input):
    letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    letter, number = input
    return int(number), letters.index(letter) + 1


def convert_output_coordinates(coordinates):
    letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    return [letters[coordinate[1]-1] + str(coordinate[0]) for coordinate in coordinates]

def convert_to_int(input_float):
    return tuple(int(x) for x in input_float)

if x_found and o_found:
    print("GAME gramy dalej")


elif not x_found:
    print("wygrał o")
elif not o_found:
    print("wygrał x")


x_positions_with_moves =[]

def finding_x_with_movement_possibility():
    # Iterowanie przez każdy rząd na szachownicy
    x_positions = []
    for i, row in enumerate(chessboard):
        # Iterowanie przez każde pole w danym rzędzie
        for j, piece in enumerate(row):
            if piece == 'x' or piece == 'X':
                x_positions.append((i, j))


    x_positions_with_moves = []
    for i, row in enumerate(chessboard):
        for j, piece in enumerate(row):
            if piece.lower() == 'x': # tworzenie pionka tymczasowego dla każdego pionka 'x'
                temp_pawn = PawnX((i, j))
                moves_x, death_pawns_x = temp_pawn.possible_moves_x(chessboard)
                if moves_x: # dodawanie pionka do x_positions tylko jeśli ma jakiekolwiek dozwolone ruchy
                    x_positions_with_moves.append((i, j))
    return x_positions_with_moves
x_positions_with_moves = finding_x_with_movement_possibility()

def finding_o_with_movement_possibility():
    # Iterowanie przez każdy rząd na szachownicy
    o_positions = []
    for i, row in enumerate(chessboard):
        # Iterowanie przez każde pole w danym rzędzie
        for j, piece in enumerate(row):
            if piece == 'o' or piece == 'O':
                o_positions.append((i, j))


    o_positions_with_moves = []
    for i, row in enumerate(chessboard):
        for j, piece in enumerate(row):
            if piece.lower() == 'o': # tworzenie pionka tymczasowego dla każdego pionka 'x'
                temp_pawn = PawnO((i, j))
                moves_o, death_pawns_o = temp_pawn.possible_moves_o(chessboard)
                if moves_o: # dodawanie pionka do x_positions tylko jeśli ma jakiekolwiek dozwolone ruchy
                    o_positions_with_moves.append((i, j))
    return o_positions_with_moves
o_positions_with_moves = finding_o_with_movement_possibility()



input_pawn_to_move = []

def input_pawn_coordinate(positions_with_moves):
    while True:
        input_pawn_to_move = input("Wybierz pionek: ")
        if str(input_pawn_to_move) in (convert_output_coordinates(positions_with_moves)):
            return input_pawn_to_move
        else:
            print("Wybrany zły pionek, wybierz inny")


def new_position(moves):

    while True:
        place_to_move = input("Podaj miejsce, gdzie sie chcesz ruszyć: ")

        if str(place_to_move) in convert_output_coordinates(moves):
            place_to_move_converted = convert_input_coordinates(place_to_move)
            break
        else:
            print("Złe dane")
    return place_to_move_converted
positon_of_beaten_pawn = []


def pawn_x_movement(pawn_new_position, pawn_position, death_pawns_x):
    l, k = pawn_new_position
    print(f"Ruszyłeś sie pionkiem x na pozycję: {convert_output_coordinates([pawn_new_position])}")
    chessboard[int(pawn_position[0])][int(pawn_position[1])] = "."
    move_without_death_paws = (pawn_position[0] + pawn_new_position[0])

    if death_pawns_x != [] and move_without_death_paws % 2 != 1:
        positon_of_beaten_pawn_float = (
            (pawn_position[0] + pawn_new_position[0]) / 2,
            (pawn_position[1] + pawn_new_position[1]) / 2
        )

        positon_of_beaten_pawn = convert_to_int(positon_of_beaten_pawn_float)
        print(
            f"Przy okazji udało Ci się zbić pionka {convert_output_coordinates([positon_of_beaten_pawn])}, masz dodatkowy ruch")

        chessboard[int(positon_of_beaten_pawn[0])][int(positon_of_beaten_pawn[1])] = "."
    else:
        positon_of_beaten_pawn = []

    if l == 1:
        chessboard[l][k] = "X"  # dopisać definicję królowki
        print("!!!!!!!!!!!!!!!!!!!zmiana na królówkę!!!!!!!!!!!!!!!!!!!!!!!")
    else:
        chessboard[l][k] = "x"
    return positon_of_beaten_pawn

def pawn_o_movement(pawn_new_position, pawn_position, death_pawns_o):
    l, k = pawn_new_position
    print(f"Ruszyłeś sie pionkiem o na pozycję: {convert_output_coordinates([pawn_new_position])}")
    chessboard[int(pawn_position[0])][int(pawn_position[1])] = "."
    move_without_death_paws = (pawn_position[0] + pawn_new_position[0])

    if death_pawns_o != [] and move_without_death_paws % 2 != 1:
        positon_of_beaten_pawn_float = (
            (pawn_position[0] + pawn_new_position[0]) / 2,
            (pawn_position[1] + pawn_new_position[1]) / 2
        )

        positon_of_beaten_pawn = convert_to_int(positon_of_beaten_pawn_float)
        print(
            f"Przy okazji udało Ci się zbić pionka {convert_output_coordinates([positon_of_beaten_pawn])}, masz dodatkowy ruch")

        chessboard[int(positon_of_beaten_pawn[0])][int(positon_of_beaten_pawn[1])] = "."
    else:
        positon_of_beaten_pawn = []

    if l == 8:
        chessboard[l][k] = "O"  # dopisać definicję królowki
        print("!!!!!!!!!!!!!!!!!!!zmiana na królówkę!!!!!!!!!!!!!!!!!!!!!!!")
    else:
        chessboard[l][k] = "o"
    return positon_of_beaten_pawn

def move_x(g):
    print_chessboard()
    print("Ruch gracza X")
    x_positions_with_moves = finding_x_with_movement_possibility()
    print(f"Pionki x, które mogą się ruszyć: {convert_output_coordinates(x_positions_with_moves)}")
    input_pawn_to_move = input_pawn_coordinate(x_positions_with_moves)
    pawn_position = convert_input_coordinates(input_pawn_to_move)
    pawn = PawnX(pawn_position)
    moves_x, death_pawns_x = pawn.possible_moves_x(chessboard)
    print("Możlwie ruch dla pionka: ", convert_output_coordinates(moves_x))
    print("Możlwie bicie: ", convert_output_coordinates(death_pawns_x))
    pawn_new_position = new_position(moves_x)
    pawn_x_movement(pawn_new_position, pawn_position, death_pawns_x)
    positon_of_beaten_pawn = None

    while positon_of_beaten_pawn != []:
        print_chessboard()
        print(f"Twój pionek znajduje się na {(convert_output_coordinates([pawn_new_position]))} ")
        move_without_death_paws_2 = (pawn_position[0] + pawn_new_position[0])
        pawn_position = pawn_new_position
        pawn = PawnX(pawn_position)
        moves_x, death_pawns_x = pawn.possible_moves_x(chessboard)

        if death_pawns_x == [] or (move_without_death_paws_2 % 2 == 1):
            break

        print("Możlwie ruch dla pionka: ", convert_output_coordinates(moves_x))
        print("Możlwie bicie: ", convert_output_coordinates(death_pawns_x))
        pawn_new_position = new_position(moves_x)
        pawn_x_movement(pawn_new_position, pawn_position, death_pawns_x)
        if pawn_new_position[0] == 1:
            print("!!!!!!!!!!!!!!!!!królówka!!!!!!!!!!!!!!!!")
        else:
            pawn_x_movement(pawn_new_position, pawn_position, death_pawns_x)

    return "o"

def move_o(g):
    print_chessboard()
    print("Ruch gracza O")
    o_positions_with_moves = finding_o_with_movement_possibility()
    print(f"Pionki o, które mogą się ruszyć: {convert_output_coordinates(o_positions_with_moves)}")
    input_pawn_to_move = input_pawn_coordinate(o_positions_with_moves)
    pawn_position = convert_input_coordinates(input_pawn_to_move)
    pawn = PawnO(pawn_position)
    moves_o, death_pawns_o = pawn.possible_moves_o(chessboard)
    print("Możlwie ruch dla pionka: ", convert_output_coordinates(moves_o))
    print("Możlwie bicie: ", convert_output_coordinates(death_pawns_o))
    pawn_new_position = new_position(moves_o)
    pawn_o_movement(pawn_new_position, pawn_position, death_pawns_o)
    positon_of_beaten_pawn = None

    while positon_of_beaten_pawn != []:
        print_chessboard()
        print(f"Twój pionek znajduje się na {(convert_output_coordinates([pawn_new_position]))} ")
        move_without_death_paws_2 = (pawn_position[0] + pawn_new_position[0])
        pawn_position = pawn_new_position
        pawn = PawnO(pawn_position)
        moves_o, death_pawns_o = pawn.possible_moves_o(chessboard)
        if death_pawns_o == [] or (move_without_death_paws_2 % 2 == 1):
            break

        print("Możlwie ruch dla pionka: ", convert_output_coordinates(moves_o))
        print("Możlwie bicie: ", convert_output_coordinates(death_pawns_o))
        pawn_new_position = new_position(moves_o)
        pawn_o_movement(pawn_new_position, pawn_position, death_pawns_o)
        if pawn_new_position[0] == 1:
            print("!!!!!!!!!!!!!!!!!królówka!!!!!!!!!!!!!!!!")
        else:
            pawn_o_movement(pawn_new_position, pawn_position, death_pawns_o)

    return "x"


def game():
    current_player = "o"

    while True:
        x_found = check_for_pieces('x')
        o_found = check_for_pieces('o')

        if x_found and o_found:

            if current_player == "x":
                current_player = move_x(current_player)
            elif current_player == "o":
                current_player = move_o(current_player)



        elif not x_found:
            print("wygrał o")
            break
        elif not o_found:
            print("wygrał x")
            break



game()


#print(positon_of_beaten_pawn) - dane zbitego pionka
#teraz mi się wydaje, że trzeba napisać funkcje poruszania się dla pionka X, czyli
""
