import csv
from pathlib import Path

path = Path.home() / "scores.csv"
def process_row(row):
    row["score"] = int(row["score"])
    return row
list_of_scores = []

with path.open(mode="r", encoding="utf-8", newline="") as file:
    reader = csv.DictReader(file)
    for row in reader:
        list_of_scores.append((process_row(row)))


max_scores = {}

for entry in list_of_scores:
    name =  entry["name"]
    score = entry["score"]

    if name in max_scores:
        max_scores[name] = max(max_scores[name], score)
    else:
        max_scores[name] = score

new_file_path = Path.home() / "high_scores.csv"

with new_file_path.open(mode="w", encoding="utf-8", newline="") as new_file:
    writer = csv.writer(new_file)

    writer.writerow(["Name", "High Score"])

    for name, score in max_scores.items():
        writer.writerow([name, score])

file.close()
