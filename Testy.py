import tkinter as tk

window = tk.Tk()
window.title("Simple text editor")

window.rowconfigure(0, minsize= 800, weight=1)
window.columnconfigure(1, minsize=800, weight=1)

txt_edit = tk.Text(window)
fr_button = tk.Frame(window)
btn_open = tk.Button(fr_button, text="Open")
btn_save = tk.Button(fr_button, text="Save as")

btn_open.grid(row=0, column=1, sticky="ew", padx=5, pady=5)
btn_save.grid(row=1, column=1, sticky="ew", padx=5)

fr_button.grid(row=0, column=0, sticky="ns")
txt_edit.grid(row=0, column=1, sticky="nsew")

window.mainloop()