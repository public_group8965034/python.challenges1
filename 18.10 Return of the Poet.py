import tkinter as tk
from tkinter.filedialog import asksaveasfilename
import random
def save_file():
    filepath = asksaveasfilename(
        defaultextension="txt",
        filetypes=[("Text Files", "*.txt"), ("All files", "*.*")]
    )
    if not filepath:
        return
    with open(filepath, "w") as output_file:
        text = lbl_poem.cget("text")
        output_file.write(text)



window = tk.Tk()
window.title("Make your own poem!")



#window.rowconfigure(0, minsize=800, weight=1)
window.columnconfigure(1, minsize=600, weight=1)

lbl_nouns = tk.Label(window, text="Nouns:")
lbl_nouns.grid(row=0, column=0, sticky="n")

ent_nouns = tk.Entry(window)
ent_nouns.grid(row=0, column=1, sticky="we", columnspan=2)

lbl_verbs = tk.Label(window, text="Verbs:")
lbl_verbs.grid(row=1, column=0, sticky="n")

ent_verbs = tk.Entry(window)
ent_verbs.grid(row=1, column=1, sticky="we", columnspan=2)

lbl_adjectives = tk.Label(window, text="Adjectives:")
lbl_adjectives.grid(row=2, column=0, sticky="n")

ent_adjectives = tk.Entry(window)
ent_adjectives.grid(row=2, column=1, sticky="we", columnspan=2)

lbl_prepositions = tk.Label(window, text="Prepositions:")
lbl_prepositions.grid(row=3, column=0, sticky="n")  # Dodane sticky

ent_prepositions = tk.Entry(window)
ent_prepositions.grid(row=3, column=1, sticky="we", columnspan=2)

lbl_adverbs = tk.Label(window, text="Adverbs:")
lbl_adverbs.grid(row=4, column=0, sticky="n")

ent_adverbs = tk.Entry(window)
ent_adverbs.grid(row=4, column=1, sticky="we", columnspan=2)

fr_generate = tk.Frame(window)
btn_generate = tk.Button(fr_generate, text="Generate")
btn_generate.grid(row=0, column=1, padx=10, pady=10, sticky="n")

fr_generate.grid(row=5, column=1)

poem = "Please fill the tables"
fr_poem = tk.Frame(window)
fr_poem.grid(row=6, column=1)
lbl_poem = tk.Label(window, text=poem)
lbl_poem.grid(row=6, column=1)

fr_save = tk.Frame(window)
fr_save.grid(row=7, column=1)
btn_save = tk.Button(fr_save, text="Save as", command=save_file)
btn_save.grid(row=7, column=1, padx=10, pady=10)


def generate_poem():
    noun = ent_nouns.get().split(",")
    verb = ent_verbs.get().split(",")
    adjective = ent_adjectives.get().split(",")
    adverb = ent_adverbs.get().split(",")
    preposition = ent_prepositions.get().split(",")

    if (
        len(noun) < 3
        or len(verb) < 3
        or len(adjective) < 3
        or len(preposition) < 2
        or len(adverb) < 1
    ):
        lbl_poem["text"] = (
            "There was a problem with your input!\n"
            "Enter at least three nouns, three verbs, three adjectives, "
            "two prepositions and an adverb!"
        )
        return

    noun1 = random.choice(noun)
    noun2 = random.choice(noun)
    noun3 = random.choice(noun)

    # Make sure that all the nouns are different
    while noun1 == noun2:
        noun2 = random.choice(noun)
    while noun1 == noun3 or noun2 == noun3:
        noun3 = random.choice(noun)


    verb1 = random.choice(verb)
    verb2 = random.choice(verb)
    verb3 = random.choice(verb)
    while verb1 == verb2:
        verb2 = random.choice(verb)
    while verb1 == verb3 or verb2 == verb3:
        verb3 = random.choice(verb)


    adj1 = random.choice(adjective)
    adj2 = random.choice(adjective)
    adj3 = random.choice(adjective)
    while adj1 == adj2:
        adj2 = random.choice(adjective)
    while adj1 == adj3 or adj2 == adj3:
        adj3 = random.choice(adjective)


    prep1 = random.choice(preposition)
    prep2 = random.choice(preposition)
    while prep1 == prep2:
        prep2 = random.choice(preposition)

    adv1 = random.choice(adverb)

    if adj1[0] in "aeiou":
        article = "An"
    else:
        article = "A"


    poem = f"{article} {adj1} {noun1}\n\n"
    poem = (
            poem + f"{article} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}\n"
    )
    poem = poem + f"{adv1}, the {noun1} {verb2}\n"
    poem = poem + f"the {noun2} {verb3} {prep2} a {adj3} {noun3}"


    lbl_poem["text"] = poem


btn_generate["command"] = generate_poem

window.mainloop()
