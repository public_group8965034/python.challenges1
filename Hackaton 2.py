class HashTable:
    def __init__(self, size=10):
        self.size = size
        # Inicjalizacja tablicy (będącej listą list dla obsługi kolizji metodą łańcuchową)
        self.table = [[] for _ in range(self.size)]

    def hash_function(self, key):
        # Metoda do obliczania indeksu w tablicy dla danego klucza
        hash_value = hash(key)
        modulo_hash = hash_value % self.size
        return modulo_hash

    def insert(self, key, value):
        # Metoda do dodawania pary klucz-wartość do tablicy haszującej
        table_index = self.hash_function(key)
        data_dict = {key: value}
        if self.contains(key):
            for dictionary in self.table[table_index]:
                if key in dictionary:
                    dictionary[key] = value
        else:
            self.table[table_index].append(data_dict)  # append automatycznie likwiduje kolizje

    def remove(self, key):
        # Metoda do usuwania pary klucz-wartość na podstawie klucza
        table_index = self.hash_function(key)
        index_to_delete = None
        if self.contains(key):
            for index, dictionary in enumerate(self.table[table_index]):
                if key in dictionary:
                    index_to_delete = index
                    break
            del self.table[table_index][index_to_delete]

    def search(self, key):
        # Metoda do wyszukiwania wartości na podstawie klucza
        table_index = self.hash_function(key)
        for dictionary in self.table[table_index]:
            for key in dictionary:
                return dictionary[key]
        return None

    def contains(self, key):
        # Metoda do sprawdzania, czy klucz znajduje się w tablicy
        table_index = self.hash_function(key)
        for dictionary in self.table[table_index]:
            for key in dictionary:
                return True
        return False

    def count_elements(self):
        # Metoda do zwracania liczby elementów w tablicy
        elements = 0
        for list_of_dict in self.table:
            elements += len(list_of_dict)
        return elements


# Przykład użycia (będzie wymagał implementacji metod)


hash_table = HashTable(size=10)
hash_table.insert("klucz1", "wartość1")
print(hash_table.search("klucz1"))
hash_table.remove("klucz1")
print(hash_table.contains("klucz1"))
print(hash_table.count_elements())
