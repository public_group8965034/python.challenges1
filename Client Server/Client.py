import socket
import json

HOST = '127.0.0.1'
PORT = 65432
BUFFER = 1024
class Client:
    def __init__(self, host, port, buffer):
        self.host = host
        self.port = port
        self.buffer = buffer

    def connect(self):
            self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.client_socket.connect((self.host, self.port))

    def send_order(self, order):
        self.order = order
        self.client_socket.sendall(order)

    def receive_msg(self):
        self.msg = self.client_socket.recv(self.buffer).decode("utf8")
        self.msg = json.loads(self.msg)
        return self.msg

    def stop_client(self):
        self.client_socket.close()

client = Client(HOST, PORT, BUFFER)
client.connect()
while True:
    order = input("Your command: ", ).encode("utf8")
    client.send_order(order)
    if order.decode("utf8") == "stop":
        client.stop_client()
        break
    recevide_msg = client.receive_msg()
    print(recevide_msg)