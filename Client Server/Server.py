import socket
import json
from datetime import datetime

HOST = '127.0.0.1'
PORT = 65432
BUFFER = 1024


class Server:
    """Creating a server."""

    def __init__(self, host, port, buffer):
        # Indication of server.
        self.json_data = None
        self.uptime = None
        self.data = None
        self.order = None
        self.addr = None
        self.conn = None
        self.server_socket = None
        self.host = host
        self.port = port
        self.buffer = buffer
        self.start_time = datetime.now()
        self.server_start_time = self.start_time.strftime("%Y-%m-%d %H:%M:%S")
        self.version = "1.0"
        self.info = (f"Server started: {self.server_start_time} \n"
                     f"Version: {self.version}")

        self.help_msg = '''\
    "uptime" - zwraca czas życia serwera
    "info" - zwraca numer wersji serwera, datę jego utworzenia
    "help" - zwraca listę dostępnych komend z krótkim opisem
    "stop" - zatrzymuje jednocześnie serwer i klienta
    '''

    def connect(self):
        # listening for client
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind((self.host, self.port))
        self.server_socket.listen()

    def accept(self):
        self.conn, self.addr = self.server_socket.accept()
        return self.conn, self.addr

    def recive_order(self):
        self.order = conn.recv(BUFFER).decode("utf8")

        if self.order == "help":
            self.data = self.help_msg
        elif self.order == "stop":
            self.data = "break"
        elif self.order == "info":
            self.data = self.info
        elif self.order == "uptime":
            self.uptime = self.get_uptime()
            self.data = f"Uptime: {self.uptime}"
        else:
            self.data = "Nieprawidłowe polecenie"

        return self.data

    def sending_json(self):
        self.json_data = json.dumps(self.data)
        self.conn.sendall(self.json_data.encode())

    def stop_server(self):
        self.server_socket.close()

    def get_uptime(self):
        self.uptime = datetime.now() - self.start_time
        return self.uptime


server = Server(HOST, PORT, BUFFER)
server.connect()

conn, addr = server.accept()
with conn:
    print("Connected by", addr)
    while True:
        json_data = server.recive_order()
        server.sending_json()
        if json_data == "break":
            server.stop_server()
            break
