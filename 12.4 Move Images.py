from pathlib import Path

old_dir = new_dir = Path.home() / "new_directory"
new_dir = Path.home() / "new_directory" / "images"
new_dir.mkdir(parents=True, exist_ok=True)

list_of_images = []
for path in old_dir.rglob("*.jpg"):
    list_of_images.append(path)

for path in old_dir.rglob("*.gif"):
    list_of_images.append(path)

for path in old_dir.rglob("*.png"):
    list_of_images.append(path)

for paths in list_of_images:
    destination_name = new_dir / paths.name
    paths.rename(destination_name)
