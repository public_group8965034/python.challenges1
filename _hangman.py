# hangman stages to print
hangman_level_1 = '''








----------
'''
hangman_level_2 = '''

   |   
   |   
   |   
   |   
   |   
   |
   |
---|------
'''
hangman_level_3 = '''
   ------
   |    
   |    
   |   
   |   
   |   
   |
   |
---|------
'''

hangman_level_4 = '''
   ------
   |    |
   |    
   |   
   |    
   |   
   |
   |
---|------
'''

hangman_level_5 = '''
   ------
   |    |
   |    O
   |   
   |    
   |   
   |
   |
---|------
'''

hangman_level_6 = '''
   ------
   |    |
   |    O
   |    |
   |    |
   |   
   |
   |
---|------
'''

hangman_level_7 = '''
   ------
   |    |
   |    O
   |   /|
   |    |
   |   
   |
   |
---|------
'''

hangman_level_8 = '''
   ------
   |    |
   |    O
   |   /|\\
   |    |
   |   
   |
   |
---|------
'''

hangman_level_9 = '''
   ------
   |    |
   |    O
   |   /|\\
   |    |
   |   / 
   |
   |
---|------
'''

hangman_level_10 = '''
   ------
   |    |
   |    O
   |   /|\\
   |    |
   |   / \\
   |
   |
---|------
'''

list_of_hangman_levels = [hangman_level_1, hangman_level_2, hangman_level_3, hangman_level_4, hangman_level_5,
                          hangman_level_6, hangman_level_7, hangman_level_8, hangman_level_9, hangman_level_10]

word = list(input("Please give the word :"))

word_guess = ""
for n in range(len(word)):
    word_guess += "_"

word_guess = list(word_guess)
print(''.join(word_guess))

given_letters = []
def give_letter():
    global given_letters
    global letter
    letter =str(input("Pleas give a letter: "))
    if len(letter) > 1:
        print("You should give only one letter")
        give_letter()
    elif letter not in "aąbcćdeęfghijklłmnńoópqrsśtuvwxyzźż":
        print("You should give a letter - no other things")
        give_letter()
    else:
        while letter in given_letters:
            print("You alredy gave this letter")
            letter = str(input("Pleas give a letter: "))

    given_letters += letter

give_letter()
hangman_level = 0

def draw_hangman(hangman_level):
    if hangman_level < 9:
        print(list_of_hangman_levels[hangman_level])
        print(''.join(word_guess))
        hangman_level += 1

    elif hangman_level == 9:
        print(hangman_level_10)
        print("Game Over YOU LOOSE")
        exit()

while True:
    if letter not in word:
        draw_hangman(hangman_level)
        give_letter()

    elif letter in word:
        for index in range(len(word)):
            if letter == word[index]:
                word_guess[index] = letter
                if ''.join(word_guess) == ''.join(word):
                    print(''.join(word_guess))
                    print("CONGRATULATION YOU WON")
                    exit()
        print(''.join(word_guess))
        give_letter()
