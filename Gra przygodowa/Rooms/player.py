class Player:
    def __init__(self, name):
        self.name = name
        self.inventory = []

    def get_name(self):
        return self.name

    def get_inventory(self):
        return self.inventory