from Rooms.player import Player
from Rooms.rooms import Rooms

name = input("Podaj swoje imię: ")
player = Player(name)
inventory = player.inventory

chest = ["klucz"]
wardrobe = ["miecz", "mop", "szalik"]

print(f"Witaj {player.name}, budzisz się w ciemnym pokoju i nie wiesz co robić. ")


while True:
    Rooms.play(player, inventory, chest, wardrobe)
