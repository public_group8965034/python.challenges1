class People:
    def __init__(self, name, surname, sex, age):
        self.name = name
        self.surname = surname
        if sex.lower() == "male" or sex.lower() == "female":
            self.sex = sex.lower()
        else:
            print("Invalid gender. Please choose 'male' or 'female'.")
            self.sex = None
        if age > 0:
            self.age = age
        else:
            print("Invalid age. Please correct.")
            self.age = None

    def introduction(self):
        if self.sex:
            print(f"Hi, my name is {self.name} {self.surname}, I'm {self.age} years old, and I'm {self.sex}.")
        else:
            print("Introduction not available due to invalid gender.")

    def __str__(self):
        return f"Name: {self.name}\n" \
               f"Surname: {self.surname}\n" \
               f"Sex: {self.sex}\n" \
               f"Age:{self.age}"

class Student(People):
    def __init__(self, name, surname, sex, age, class_num, building_acces = False):
        super().__init__(name, surname, sex, age)
        self.class_num = class_num
        self.building_acces = building_acces

    def __str__(self):
        peopple_str = super().__str__()
        return f"{peopple_str} \nBuilding Acces: {self.building_acces}"

class Teacher(People):
    def __init__(self, name, surname, sex, age, grade_acces = True, building_acces = True):
        super().__init__(name, surname, sex, age)
        self.grade_acces = grade_acces
        self.building_acces = building_acces

    def __str__(self):
        peopple_str = super().__str__()
        return f"{peopple_str} \nGrades Acces: {self.grade_acces} \nBuilding Acces: {self.building_acces}"

class Director(People):
    def __init__(self, name, surname, sex, age, grade_acces = True, building_acces = True, teachers_salary_acces = True):
        super().__init__(name, surname, sex, age)
        self.class_num = grade_acces
        self.building_acces = building_acces
        self.grade_acces = grade_acces
        self.teachers_salary_acces = teachers_salary_acces
    def __str__(self):
        peopple_str = super().__str__()
        return f"{peopple_str} \nGrades Acces: {self.grade_acces} \nBuilding Acces: {self.building_acces}" \
               f"\nTeachers Salary Acces: {self.teachers_salary_acces}"

class Janitor(People):
    def __init__(self, name, surname, sex, age, building_acces = True):
        super().__init__(name, surname, sex, age)
        self.building_acces = building_acces

    def __str__(self):
        peopple_str = super().__str__()
        return f"{peopple_str} \nGrades Acces: {self.grade_acces} \nBuilding Acces: {self.building_acces}"


student1 = Student("Jan", "Kowalski", "male", 14, "3A")
teacher1 = Teacher("Alicja", "Stasiak", "female", 35,)
director1 = Director("Adam", "Nowak", "male", 55)
janitor1 = Janitor("Joanna", "Kania", "female", 45)

student1.introduction()
teacher1.introduction()
director1.introduction()
janitor1.introduction()


print("\nStudent:")
print(student1)

print("\nTeacher:")
print(teacher1)

print("\nDirector:")
print(director1)

print("\nJanitor:")
print(janitor1)
