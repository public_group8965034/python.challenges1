class Player:
    def __init__(self, name):
        self.name = name
        self.inventory = []


name = input("Podaj swoje imię: ")
player = Player(name)
inventory = player.inventory

commends ="co mam - wyświetla zawartość ekwipunku\n" \
         "obczaj - rozglądasz się po pomieszczeniu\n" \
          "użyj - używa przedmiotu\n" \
          "idź do = idziesz do danego miejsca\n"

chest = ["klucz"]
wardrobe = ["miecz", "mop", "szalik"]

print(f"Witaj {player.name}, budzisz się w ciemnym pokoju i nie wiesz co robić. ")

nap = 0
room = "dark"

while True:
    if room == "dark":
        what_to_do = input("Co chcesz zrobić? (jak nie wiesz co zrobić, wpisz: pomoc) ")

        if what_to_do == "pomoc":
            print(commends)
        elif what_to_do == "co mam":
            print("Mam: ", player.inventory)
        elif what_to_do == "idź do":
            print("Nie masz gdzie iść bo jest ciemno i nic nie widzisz")
        elif what_to_do == "obczaj":
            print("Rozglądasz się po pokoju, jest ciemno, nic nie widzisz. Wyczuwasz pod palcami wyłącznik swiatła.")
            what_to_do = input("Możesz spać dalej (użyj polecenia 'śpij') lub włączyć światło (użyj polecenia 'włącz'): ")

            if what_to_do == "śpij" and nap < 2:
                    print("O drzemka dobrze Ci zrobi, tylko nie śpij za długo")
                    nap += 1
            elif what_to_do == "śpij" and nap >= 2:
                print("Umierasz.")
                break
            elif what_to_do == "włącz":
                 print("Włączam światło, widzę pokój z drzwiami. Na środku pokoju znajduje się skrzynia, w rogu pokoju szafa")
                 room = "light"
        else:
            print("Zła komenda")

    elif room == "light":
        go_to = input("Widzisz: skrzynię, drzwi i szafę. Gdzie chcesz podejść? "
                       "Użyj polecenia 'idź do' ")
        if go_to == "co mam":
            print("Mam: ", player.inventory)
        elif go_to == "użyj szalik" and "szalik" in inventory:
             print("Ubierasz szalik, jest Ci cieplej")
        elif go_to == "użyj mop" and "mop" in inventory:
             print("Machasz mopem, nic się nie dzieje")
        elif go_to == "idź do szafa" or go_to == "idź do szafy":
             print(f"Podchodzisz do szafy, w środku znajduje się: {wardrobe} ")

             wardrobe_action = input("Co chcesz zrobić? (pamiętaj o poleceniu 'weź')")
             if wardrobe_action == "weź mop":
                 if "mop" in wardrobe:
                     wardrobe.remove("mop")
                     inventory.append("mop")
                     print("Wziąłeś mop")
                 else:
                     print("Nie ma mopa w szafie, już go wziąłeś.")
             elif wardrobe_action == "weź szalik":
                 if "szalik" in wardrobe:
                     wardrobe.remove("szalik")
                     inventory.append("szalik")
                     print("Wziąłeś szalik")
             elif wardrobe_action == "weź miecz":
                 if "szalik" in wardrobe:
                     wardrobe.remove("miecz")
                     inventory.append("miecz")
                     print("Wziąłeś miecz")
                 else:
                     print("Nie ma miecza w szafie, już go wziąłeś.")
             else:
                 print("Zła komenda")


        elif go_to == "idź do skrzynia" or go_to == "idź do skrzyni":
             print(f"Podchodzisz do skrzyni, w środku znajduje się: {chest}.")
             chest_action = input("Co chesz zrobić? (pamiętaj o poleceniu 'weź') ")
             if chest_action == "weź klucz":
                 if "klucz" in chest:
                     chest.remove("klucz")
                     inventory.append("klucz")
                     print("Wziąłeś klucz")
                 else:
                     print("Skrzynia jest pusta")
             else:
                 print("Zła komenda")

        elif go_to == "idź do drzwi":
             print("Podchodzisz do drzwi. Drzwi są zamknięte.")
             door_action = input("Co chcesz zrobić ")
             if door_action == "użyj klucz" and "klucz" in inventory:
                 print("Użyłeś klucza do otwarcia drzwi, przechodzisz do nwoego pokoju")
                 room = "beast"
    elif room == "beast":
        what_to_do_2 = input("co chcesz zrobić? ")
        if what_to_do_2 == "idź do drzwi" and "złoty klucz" not in inventory:
                    print("Próbujesz zakraść się do drzwi, podchodzisz...., ciągniesz za klamkę, jednak drzwi"
                       "są zamknięte. Nie zdajesz sobie sprawy, ale hałas obudził beboka. atakuje Cię"
                       "z zaskoczenia, UMIERASZ!!!")
        elif what_to_do_2 == "obczaj":
            print("Na środku pokoju jest bebok, który strzeże złotych drzwi, co robisz?")
        elif what_to_do_2 == "idź do drzwi" and "złoty klucz" not in inventory:
            print("Próbujesz zakraść się do drzwi, podchodzisz...., ciągniesz za klamkę, jednak drzwi"
                           "są zamknięte. Nie zdajesz sobie sprawy, ale hałas obudził beboka. atakuje Cię"
                           "z zaskoczenia, UMIERASZ!!!")

            break
        elif what_to_do_2 == "użyj miecz" and "miecz" in inventory:
            print("Wyciągasz miecz i zaczynasz walczyć. Po cieżkiej walce wygrywasz. Zauważasz na"
                           "jego szyi złoty klucz, bieżesz go do swojego plecaka ")
            inventory.append("złoty klucz")
            last_action = input("Użyj złotego klucza")
            if last_action == "użyj złoty klucz":
                print("Gratulacje WYGRAŁEŚ!!!")
                break
        elif what_to_do_2 == "pomoc":
            print(commends)
        elif what_to_do_2 == "co mam":
            print(f"Mam {inventory} ")

        elif door_action == "użyj klucza" and "klucz" not in inventory:
            print("Nie masz klucza")
        else:
            print("Zła komedia")

