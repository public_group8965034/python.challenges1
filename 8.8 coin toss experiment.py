import random

def coin_flip():
    #randomnly return heads or tails
    if random.randint(0, 1) == 1:
        return "heads"
    else:
        return "tails"
def how_many():
    same_side = coin_flip()
    new_flip = ""
    x = 1

    while same_side != new_flip:
        new_flip = coin_flip()
        x += 1
    return x
w = 0

for n in range(1000):
    y = how_many()
    w = w + y
print(w / 1000)
