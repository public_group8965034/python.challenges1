chessboard = [
    ['0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H','0'],
    ['1', '.', 'o', '.', 'o', '.', 'o', '.', 'o','1'],
    ['2', 'o', '.', 'o', '.', 'o', '.', 'o', '.','2'],
    ['3', '.', '.', '.', '.', '.', '.', '.', '.','3'],
    ['4', '.', '.', '.', '.', '.', '.', '.', '.','4'],
    ['5', '.', '.', '.', '.', '.', '.', '.', '.','5'],
    ['6', '.', '.', '.', '.', '.', '.', '.', '.','6'],
    ['7', '.', 'x', '.', 'x', '.', 'x', '.', 'x','7'],
    ['8', 'x', '.', 'x', '.', 'x', '.', 'x', '.','8'],
    ['0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H','0']
]

from warcaby_moduly.queen import Queen
from warcaby_moduly.pawns import PawnX, PawnO

def print_chessboard():
    for line in chessboard:
        print(' '.join(line))

def check_for_pieces(piece):
    return any(piece in row or piece.upper() in row for row in chessboard)

x_found = check_for_pieces('x') #sprawdza, czy są małe bądz duże X na planszy
o_found = check_for_pieces('o')

def convert_input_coordinates(input):
    letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    letter, number = input
    return int(number), letters.index(letter) + 1

def convert_output_coordinates(coordinates):
    letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    return [letters[coordinate[1]-1] + str(coordinate[0]) for coordinate in coordinates]

def convert_to_int(input_float):
    return tuple(int(x) for x in input_float)

if x_found and o_found:
    print("Gramy")

elif not x_found:
    print("Gratulacje wygrał gracz O")
elif not o_found:
    print("Gratulacje wygrał gracz X")

x_positions_with_moves =[]

def finding_x_with_movement_possibility():
    x_positions = []
    for i, row in enumerate(chessboard):
        for j, piece in enumerate(row):
            if piece == 'x' or piece == 'X':
                x_positions.append((i, j))

    x_positions_with_moves = []
    for i, row in enumerate(chessboard):
        for j, piece in enumerate(row):
            if piece == 'x': # tworzenie pionka tymczasowego dla każdego pionka 'x'
                temp_pawn = PawnX((i, j))
                moves_x, death_pawns_x = temp_pawn.possible_moves_x(chessboard)
                if moves_x: # dodawanie pionka do x_positions tylko jeśli ma jakiekolwiek dozwolone ruchy
                    x_positions_with_moves.append((i, j))
            if piece == 'X':
                temp_queen = Queen((i, j), "x")
                temp_queen_possible_kills, temp_queen_possible_moves = temp_queen.all_possible_moves_for_queen(chessboard, "o")
                if temp_queen_possible_moves:
                    x_positions_with_moves.append((i, j))

    return x_positions_with_moves
x_positions_with_moves = finding_x_with_movement_possibility()

def finding_o_with_movement_possibility():
    o_positions = []
    for i, row in enumerate(chessboard):
        for j, piece in enumerate(row):
            if piece == 'o' or piece == 'O':
                o_positions.append((i, j))

    o_positions_with_moves = []
    for i, row in enumerate(chessboard):
        for j, piece in enumerate(row):
            if piece == 'o':
                temp_pawn = PawnO((i, j))
                moves_o, death_pawns_o = temp_pawn.possible_moves_o(chessboard)
                if moves_o:
                    o_positions_with_moves.append((i, j))
            if piece == 'O':
                temp_queen = Queen((i, j), "o")
                temp_queen_possible_kills, temp_queen_possible_moves = temp_queen.all_possible_moves_for_queen(
                    chessboard, "x")
                if temp_queen_possible_moves:
                    o_positions_with_moves.append((i, j))
    return o_positions_with_moves

o_positions_with_moves = finding_o_with_movement_possibility()

input_pawn_to_move = []

def input_pawn_coordinate(positions_with_moves):
    while True:
        input_pawn_to_move = input("Wybierz pionek: ")
        if str(input_pawn_to_move) in (convert_output_coordinates(positions_with_moves)):
            return input_pawn_to_move
        else:
            print("Wybrany zły pionek, wybierz inny")

def new_position(moves):

    while True:
        place_to_move = input("Podaj miejsce, gdzie sie chcesz ruszyć: ")
        if str(place_to_move) in convert_output_coordinates(moves):
            place_to_move_converted = convert_input_coordinates(place_to_move)
            break
        else:
            print("Złe dane")

    return place_to_move_converted

positon_of_beaten_pawn = []


def pawn_x_movement(pawn_new_position, pawn_position, death_pawns_x):
    l, k = pawn_new_position
    print(f"Ruszyłeś sie pionkiem x na pozycję: {convert_output_coordinates([pawn_new_position])}")
    chessboard[int(pawn_position[0])][int(pawn_position[1])] = "."
    move_without_death_paws = (pawn_position[0] + pawn_new_position[0])

    if death_pawns_x != [] and move_without_death_paws % 2 != 1:
        positon_of_beaten_pawn_float = (
            (pawn_position[0] + pawn_new_position[0]) / 2,
            (pawn_position[1] + pawn_new_position[1]) / 2
        )

        positon_of_beaten_pawn = convert_to_int(positon_of_beaten_pawn_float)
        print(
            f"Przy okazji udało Ci się zbić pionka {convert_output_coordinates([positon_of_beaten_pawn])}")

        chessboard[int(positon_of_beaten_pawn[0])][int(positon_of_beaten_pawn[1])] = "."
    else:
        positon_of_beaten_pawn = []

    if l == 1:
        chessboard[l][k] = "X"
        print("Zmiana na królową")
    else:
        chessboard[l][k] = "x"
    return positon_of_beaten_pawn

def pawn_o_movement(pawn_new_position, pawn_position, death_pawns_o):
    l, k = pawn_new_position
    print(f"Ruszyłeś sie pionkiem o na pozycję: {convert_output_coordinates([pawn_new_position])}")
    chessboard[int(pawn_position[0])][int(pawn_position[1])] = "."
    move_without_death_paws = (pawn_position[0] + pawn_new_position[0])

    if death_pawns_o != [] and move_without_death_paws % 2 != 1:
        positon_of_beaten_pawn_float = (
            (pawn_position[0] + pawn_new_position[0]) / 2,
            (pawn_position[1] + pawn_new_position[1]) / 2
        )

        positon_of_beaten_pawn = convert_to_int(positon_of_beaten_pawn_float)
        print(
            f"Przy okazji udało Ci się zbić pionka {convert_output_coordinates([positon_of_beaten_pawn])}")

        chessboard[int(positon_of_beaten_pawn[0])][int(positon_of_beaten_pawn[1])] = "."
    else:
        positon_of_beaten_pawn = []

    if l == 8:
        chessboard[l][k] = "O"
        print("Zmiana na królową")
    else:
        chessboard[l][k] = "o"
    return positon_of_beaten_pawn

def move_x(g):
    print_chessboard()
    print("Ruch gracza X")
    x_positions_with_moves = finding_x_with_movement_possibility()
    print(f"Pionki x, które mogą się ruszyć: {convert_output_coordinates(x_positions_with_moves)}")
    input_pawn_to_move = input_pawn_coordinate(x_positions_with_moves)
    pawn_position = convert_input_coordinates(input_pawn_to_move)
    t, s = pawn_position
    if chessboard[t][s] == "x":
        pawn = PawnX(pawn_position)
        moves_x, death_pawns_x = pawn.possible_moves_x(chessboard)
        print("Możlwie ruch dla pionka: ", convert_output_coordinates(moves_x))
        print("Możlwie bicie: ", convert_output_coordinates(death_pawns_x))
        pawn_new_position = new_position(moves_x)
        pawn_x_movement(pawn_new_position, pawn_position, death_pawns_x)
        positon_of_beaten_pawn = None

        while positon_of_beaten_pawn != []:

            print(f"Twój pionek znajduje się na {(convert_output_coordinates([pawn_new_position]))} ")
            move_without_death_paws_2 = (pawn_position[0] + pawn_new_position[0])
            pawn_position = pawn_new_position
            pawn = PawnX(pawn_position)
            moves_x, death_pawns_x = pawn.possible_moves_x(chessboard)

            if death_pawns_x == [] or (move_without_death_paws_2 % 2 == 1):
                break
            print_chessboard()
            print("Możlwie ruch dla pionka: ", convert_output_coordinates(moves_x))
            print("Możlwie bicie: ", convert_output_coordinates(death_pawns_x))
            pawn_new_position = new_position(moves_x)
            pawn_x_movement(pawn_new_position, pawn_position, death_pawns_x)
            if pawn_new_position[0] == 1:
                print("Zmiana na królową")
            else:
                pawn_x_movement(pawn_new_position, pawn_position, death_pawns_x)
    elif chessboard[t][s] == "X":
        queen = Queen(pawn_position, "x")
        queen_enemy_colour = "o"
        queen_possible_kills, queen_possible_moves = queen.all_possible_moves_for_queen(chessboard, queen_enemy_colour)
        print("Możlwie ruch dla królowej: ", convert_output_coordinates(queen_possible_moves))
        print("Możlwie bicie dla królowej: ", convert_output_coordinates(queen_possible_kills))
        queen_new_position = new_position(queen_possible_moves)
        print(f"Ruszyłeś sie królową X na pozycję: {convert_output_coordinates([queen_new_position])}")
        killed_pawns_result = queen.queen_movement(chessboard, queen_new_position,pawn_position, "x", "o")

        while killed_pawns_result != []:

            print(f"Ruszyłeś sie królową X na pozycję: {convert_output_coordinates([queen_new_position])}")
            queen = Queen(queen_new_position, "x")
            pawn_position = queen_new_position
            queen_possible_kills, queen_possible_moves = queen.all_possible_moves_for_queen(chessboard,
                                                                                            queen_enemy_colour)
            if queen_possible_kills == []:
                break
            print_chessboard()
            print("Możlwie ruch dla królowej: ", convert_output_coordinates(queen_possible_moves))
            print("Możlwie bicie dla królowej: ", convert_output_coordinates(queen_possible_kills))
            queen_new_position = new_position(queen_possible_moves)
            print(f"Ruszyłeś sie królową X na pozycję: {convert_output_coordinates([queen_new_position])}")  #to do wywalenia bo tu sie zapętla i wydrukuje to wyżej
            killed_pawns_result = queen.queen_movement(chessboard, queen_new_position, pawn_position, "x", "o")
    return "o"

def move_o(g):
    print_chessboard()
    print("Ruch gracza O")
    o_positions_with_moves = finding_o_with_movement_possibility()
    print(f"Pionki o, które mogą się ruszyć: {convert_output_coordinates(o_positions_with_moves)}")
    input_pawn_to_move = input_pawn_coordinate(o_positions_with_moves)
    pawn_position = convert_input_coordinates(input_pawn_to_move)
    t, s = pawn_position
    if chessboard[t][s] == "o":
        pawn = PawnO(pawn_position)
        moves_o, death_pawns_o = pawn.possible_moves_o(chessboard)
        print("Możlwie ruch dla pionka: ", convert_output_coordinates(moves_o))
        print("Możlwie bicie: ", convert_output_coordinates(death_pawns_o))
        pawn_new_position = new_position(moves_o)
        pawn_o_movement(pawn_new_position, pawn_position, death_pawns_o)
        positon_of_beaten_pawn = None

        while positon_of_beaten_pawn != []:

            print(f"Twój pionek znajduje się na {(convert_output_coordinates([pawn_new_position]))} ")
            move_without_death_paws_2 = (pawn_position[0] + pawn_new_position[0])
            pawn_position = pawn_new_position
            pawn = PawnO(pawn_position)
            moves_o, death_pawns_o = pawn.possible_moves_o(chessboard)
            if death_pawns_o == [] or (move_without_death_paws_2 % 2 == 1):
                break
            print_chessboard()
            print("Możlwie ruch dla pionka: ", convert_output_coordinates(moves_o))
            print("Możlwie bicie: ", convert_output_coordinates(death_pawns_o))
            pawn_new_position = new_position(moves_o)
            pawn_o_movement(pawn_new_position, pawn_position, death_pawns_o)
            if pawn_new_position[0] == 1:
                print("Zmiana na królową")
            else:
                pawn_o_movement(pawn_new_position, pawn_position, death_pawns_o)
    elif chessboard[t][s] == "O":
        queen = Queen(pawn_position, "o")
        queen_enemy_colour = "x"
        queen_possible_kills, queen_possible_moves = queen.all_possible_moves_for_queen(chessboard, queen_enemy_colour)
        print("Możlwie ruch dla królowej: ", convert_output_coordinates(queen_possible_moves))
        print("Możlwie bicie dla królowej: ", convert_output_coordinates(queen_possible_kills))
        queen_new_position = new_position(queen_possible_moves)
        print(f"Ruszyłeś sie królową O na pozycję: {convert_output_coordinates([queen_new_position])}")
        killed_pawns_result = queen.queen_movement(chessboard, queen_new_position, pawn_position, "o", "x")

        while killed_pawns_result != []:
            queen = Queen(queen_new_position, "o")
            pawn_position = queen_new_position
            queen_possible_kills, queen_possible_moves = queen.all_possible_moves_for_queen(chessboard,
                                                                                            queen_enemy_colour)
            if queen_possible_kills == []:
                break
            print_chessboard()

            print("Możlwie ruch dla królowej: ", convert_output_coordinates(queen_possible_moves))
            print("Możlwie bicie dla królowej: ", convert_output_coordinates(queen_possible_kills))
            queen_new_position = new_position(queen_possible_moves)

            killed_pawns_result = queen.queen_movement(chessboard, queen_new_position, pawn_position, "o", "x")
            queen_possible_moves_with_kill = [move for move in queen_possible_moves if move in queen_possible_kills]

    return "x"

def game():
    current_player = "o"

    while True:
        x_found = check_for_pieces('x')
        o_found = check_for_pieces('o')

        if x_found and o_found:

            if current_player == "x":
                current_player = move_x(current_player)
            elif current_player == "o":
                current_player = move_o(current_player)

        elif not x_found:
            print("wygrał o")
            break
        elif not o_found:
            print("wygrał x")
            break

game()
#to jest test do gita
