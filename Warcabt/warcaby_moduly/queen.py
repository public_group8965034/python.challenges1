
class Queen:
    def __init__(self, queen_position, queen_colour):
        self.i, self.j = queen_position
        self.queen_colour = queen_colour

    def current_position(self):
        position = (self.i, self.j)
        return position

    def possible_moves(self, chessboard, direction):
        i, j = self.i, self.j
        moves = {}

        # Definicje kierunków (indeksy zmian w pozycji)
        directions = {
            'NE': (-1, 1),
            'SE': (1, 1),
            'SW': (1, -1),
            'NW': (-1, -1)
        }

        di, dj = directions[direction]

        while True:
            i, j = i + di, j + dj

            if 1 <= i < (len(chessboard) - 1) and 1 <= j < (len(chessboard[0]) - 1):
                current_field = chessboard[i][j]
                moves[(i, j)] = current_field
            else:
                break
        return moves

    def cutting_moves_dict(self, moves, queen_enemy_colour):
        cut_moves = {}
        previous_value = None
        all_keys = list(moves.keys())

        for key, value in moves.items():
            if (all_keys.index(key) + 1 < len(all_keys) and moves[all_keys[all_keys.index(
                    key) + 1]].lower() != "." and value.lower() == queen_enemy_colour) or value.lower() == self.queen_colour.lower():
                break
            cut_moves[key] = value
            previous_value = value.lower()

        return cut_moves


    def possible_kills_for_queen(self, cut_moves, queen_enemy_colour):
        possible_kills = []

        for key, value in cut_moves.items():
            if key == list(cut_moves.keys())[-1]:
                break
            if value.lower() == queen_enemy_colour.lower():
                possible_kills.append(key)
                pass
        return possible_kills

    def possible_moves_for_queen(self, cut_moves, queen_enemy_colour):
        possible_moves = []
        for key, value in cut_moves.items():
            if value == '.':
                possible_moves.append(key)
        return possible_moves

    def all_possible_moves_for_queen(self, chessboard, queen_enemy_colour):
        directions = ["NE", "SE", "SW", "NW"]
        queen_possible_kills = []
        queen_possible_moves = []
        queen_possible_moves_with_kill = []

        for direction in directions:
            moves_dictionary = self.possible_moves(chessboard, direction)
            moves_temp = self.cutting_moves_dict(moves_dictionary, queen_enemy_colour)
            queen_possible_kills += self.possible_kills_for_queen(moves_temp, queen_enemy_colour)
            queen_possible_moves += self.possible_moves_for_queen(moves_temp, queen_enemy_colour)

        return queen_possible_kills, queen_possible_moves

    def queen_movement(self, chessboard, queen_new_position, pawn_position, queen_colour, queen_enemy_colour):
        l, k = queen_new_position
        m,n = pawn_position
        chessboard[m][n] = "."
        chessboard[l][k] = queen_colour.upper()
        killed_pawns = []
        if l < m and k < n:
            while l < m and k < n:

                if (chessboard[l][k]).lower() == queen_enemy_colour:
                    killed_pawns.append((l,k))
                    chessboard[l][k] = "."
                l += 1
                k += 1

        if l > m and k < n:
            while l > m and k < n:

                if (chessboard[l][k]).lower() == queen_enemy_colour:
                    killed_pawns.append((l, k))
                    chessboard[l][k] = "."
                l -= 1
                k += 1
        if l < m and k > n:
            while l < m and k > n:

                if (chessboard[l][k]).lower() == queen_enemy_colour:
                    killed_pawns.append((l, k))
                    chessboard[l][k] = "."
                l += 1
                k -= 1
        if l > m and k > n:
            while l > m and k > n:

                if (chessboard[l][k]).lower() == queen_enemy_colour:
                    killed_pawns.append((l, k))
                    chessboard[l][k] = "."
                l -= 1
                k -= 1

        return killed_pawns



# ta funkcja musi informować na jakie miejsce się ruszyłem, zamienic na kropkę pawn position, jezeli było bicie
# to również tam ma zmienic na kropkę oraz dać dodatkowy ruch, przy okazji musi mi zwrócić pozycję zbitych pionków
