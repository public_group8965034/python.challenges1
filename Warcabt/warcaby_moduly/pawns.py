
class PawnX:
    def __init__(self, pawn_position):
        self.i, self.j = pawn_position

    def current_position(self):
        position = (self.i, self.j)
        return position

    def possible_moves_x(self, chessboard):

        moves_x = []  # lista na możliwe ruchy
        death_pawns_x = [] # lista ruchów z możliwymi biciami

        if self.j > 1 and chessboard[self.i - 1][self.j - 1] == '.':
            moves_x.append((self.i - 1, self.j - 1))

        if self.j < 8 and chessboard[self.i - 1][self.j + 1] == '.':
            moves_x.append((self.i - 1, self.j + 1))

        if self.i > 1 and self.j > 1 and (chessboard[self.i - 1][self.j - 1].lower() == 'o') and chessboard[self.i - 2][
            self.j - 2] == '.':
            moves_x.append((self.i - 2, self.j - 2))
            death_pawns_x.append((self.i - 1, self.j - 1))

        if self.i > 1 and self.j < 7 and (chessboard[self.i - 1][self.j + 1].lower() == 'o') and chessboard[self.i - 2][
            self.j + 2] == '.':
            moves_x.append((self.i - 2, self.j + 2))
            death_pawns_x.append((self.i - 1, self.j + 1))

        return moves_x, death_pawns_x

class PawnO:
    def __init__(self, pawn_position):
        self.i, self.j = pawn_position

    def current_position(self):
        position = (self.i, self.j)
        return position

    def possible_moves_o(self, chessboard):

        moves_o = []
        death_pawns_o = []

        if self.j > 1 and chessboard[self.i + 1][self.j - 1] == '.':
            moves_o.append((self.i + 1, self.j - 1))

        if self.j < 8 and chessboard[self.i + 1][self.j + 1] == '.':
            moves_o.append((self.i + 1, self.j + 1))

        if self.i < 7 and self.j < 7 and (chessboard[self.i + 1][self.j + 1].lower() == 'x') and chessboard[self.i + 2][
            self.j + 2] == '.':
            moves_o.append((self.i + 2, self.j + 2))
            death_pawns_o.append((self.i + 1, self.j + 1))

        if self.i > 0 and self.j < 7 and (chessboard[self.i + 1][self.j - 1].lower() == 'x') and chessboard[self.i + 2][
            self.j - 2] == '.':
            moves_o.append((self.i + 2, self.j - 2))
            death_pawns_o.append((self.i + 1, self.j - 1))
        return moves_o, death_pawns_o
