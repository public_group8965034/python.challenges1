import tkinter as tk

class ChessboardApp:
    def __init__(self, master):
        self.master = master
        self.master.title("Chessboard")

        self.board_size = 8
        self.create_chessboard()

    def create_chessboard(self):
        for row in range(self.board_size):
            for col in range(self.board_size):
                color = "white" if (row + col) % 2 == 0 else "black"
                square = tk.Canvas(
                    self.master,
                    width=50,
                    height=50,
                    background=color,
                    borderwidth=0,
                    highlightthickness=0,
                )
                square.grid(row=row, column=col)

if __name__ == "__main__":
    root = tk.Tk()
    app = ChessboardApp(root)
    root.mainloop()