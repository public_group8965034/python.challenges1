import socket
import json

class Client:
    def __init__(self, host, port, buffer):
        self.msg = None
        self.order = None
        self.client_socket = None
        self.host = host
        self.port = port
        self.buffer = buffer

    def connect(self):
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.connect((self.host, self.port))

    def send_order(self, order):
        self.order = order
        self.client_socket.sendall(order)

    def receive_msg(self):
        self.msg = self.client_socket.recv(self.buffer).decode("utf8")
        self.msg = json.loads(self.msg)
        return self.msg

    def stop_client(self):
        self.client_socket.close()


