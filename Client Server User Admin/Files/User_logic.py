import json


class JsonLogic:
    """user database management"""

    def __init__(self):
        self.json_file = "Files/user_data.json"
        self.username = None
        self.data = None
        self.load_data()

    def load_data(self):
        with open(self.json_file, 'r') as file:
            self.data = json.load(file)

    def check_password(self, password):
        for name in self.data:
            if name["username"] == self.username:
                if name["password"] == password:
                    return True
                else:
                    return False
        print("brak użytkownika")

    def login(self):
        self.username = input("Enter your username: ")
        password = input("Enter your password: ")
        return self.check_password(password)

    def check_access(self):
        for name in self.data:
            if name["username"] == self.username:
                if name["type"] == "admin":
                    return True



