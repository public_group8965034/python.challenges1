from Files.Client_class import Client

HOST = '127.0.0.1'
PORT = 65432
BUFFER = 1024



client = Client(HOST, PORT, BUFFER)
client.connect()
while True:
    order = input("Your command: ", ).encode("utf8")
    client.send_order(order)
    if order.decode("utf8") == "stop":
        client.stop_client()
        break
    received_msg = client.receive_msg()
    print(received_msg)
