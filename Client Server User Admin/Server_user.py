from Files.Server_class import Server


HOST = '127.0.0.1'
PORT = 65432
BUFFER = 1024

server = Server(HOST, PORT, BUFFER)
server.connect()

conn, addr = server.accept()
with conn:
    print("Connected by", addr)
    while True:
        json_data = server.receive_order()
        server.sending_json()
        if json_data == "break":
            server.stop_server()
            break
