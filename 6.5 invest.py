def invest(ammount, rate, years):
    for years in range(years):
        ammount = ammount + ammount * rate/100
        print(f"year {years + 1} : {ammount:,.2f}")

ammount = float(input("What is your principal ammount? "))
rate = float(input("What is your percentage rate? "))
years = int(input("What is your number of years? "))

invest(ammount, rate, years)
