queue = {
    1: 3,
    2: 4,
    3: 1,
    4: 1,
    5: 2,
    6: 5,
    7: 4,
    8: 2,
    9: 3,
}


def new_person(queue, priorytet): #dodaje osobę na koniec kolejki
    new_number = max(queue.keys(), default=0)+ 1
    queue[new_number] = priorytet
    return queue

def whos_first(queue): # sprawdza kto jest pierwszy w kolejce

    for person, priorytet in queue.items():
        first_person = {person, priorytet}
        return first_person
        break

first_person_in_line = whos_first(queue)

def remove_first_person(queue): #usuwa pierwszą osobę z kolejki (bez zwracania uwagi na priorytet)
    for key in (whos_first(queue)):
        del queue[key]
    return queue

def queue_empty(queue): #sprawdza czy kolejka jest pusta
    if len(queue) == 0:
        return False
    else:
        return True

def how_long(queue): #sprawdza długość kolejki
    return len(queue)

def remove_prior_queue(queue): #usuwa pierwszą osobą o najwyższym priorytecie
    keys = []
    for i in range(5, 0, -1):
        for key, value in queue.items():
            if value == i:
                keys.append(key)
        if len(keys) > 0:
            del queue[keys[0]]
            break
    return queue


new_person(queue, 4)
new_person(queue, 1)

print(queue_empty(queue))

print(how_long(queue))

print(queue)

remove_prior_queue(queue)

print(queue)