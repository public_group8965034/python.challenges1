cats = {}
#False = no hat
#True = hat

for n in range(1, 101) :
    cats[n] = False
    n += 1

for k in range(1, 101):

    cat_num = k
    while cat_num < 101:
        cats[cat_num] = not cats[cat_num]
        cat_num += k

cats_with_hats = []

for cat, hat in cats.items():
    if hat:
        cats_with_hats.append(cat)
print(f"Cats with hats are: {cats_with_hats}")
