universities = [
    ["California Institiute of Technology", 2175, 37704],
    ["Harvard", 19627, 39849],
    ["Massachisetts Institute of Technology", 10566, 40732],
    ["Princeton", 7802, 37000],
    ["Rice", 5879, 35551],
    ["Stanford", 19535, 40569],
    ["Yale", 11701, 40500]
]

def enrollment_stats():
    n = 0
    student_tuition_fees = []
    student_enrollment_values = []
    for universiy in range(len(universities)):
        student_enrollment_values.append(universities[n][1])
        student_tuition_fees.append(universities[n][2])
        n += 1
    return student_enrollment_values, student_tuition_fees

x = 0
def mean(x):
    mean_of_x = ((sum(enrollment_stats()[x])) / (len(enrollment_stats()[x])))
    return mean_of_x

import math
y = 0
def median(y):
    median_of_y = (sorted((enrollment_stats()[y])))
    return median_of_y[(math.floor((len(enrollment_stats()[y]) / 2)))]


print(f"Total students: {sum(enrollment_stats()[0]):,}")
print(f"Total tuition: $ {sum(enrollment_stats()[1]):,}")
print()
print(f"Student mean: {mean(0):,.2f}")
print(f"Student median: $ {median(0):,.2f}")
print()
print(f"Tuition mean: $ {mean(1):,.2f}")
print(f"Tuiton median: $ {median(1):,.2f}")
