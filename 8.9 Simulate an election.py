import random

def voting_results_reg(probability_of_reg):
    if random.random() < probability_of_reg:
        return 1
    else:
        return 0
results = 0
for voting in range(10000):
    x = voting_results_reg(.87)
    y = voting_results_reg(.65)
    z = voting_results_reg(.17)
    can_a_won = 0
    can_b_won = 0

    if x + y + z >= 2:
        can_a_won += 1
    else:
        can_b_won += 1
    results = results + can_a_won
print(f"Candidat A won {results} times out of 1000")
