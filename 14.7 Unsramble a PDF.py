from pathlib import Path

from PyPDF2 import PdfReader, PdfWriter


def get_page_text(page):
    return page.extract_text()

pdf_path = (
    Path.home() /
    "Documents" /
    "practice_files" /
    "scrambled.pdf"
)

pdf_reader = PdfReader(str(pdf_path))
pdf_writer = PdfWriter()

pages = list(pdf_reader.pages)
pages.sort(key=get_page_text)

for page in pages:
    if page["/Rotate"] != 0:
        x = 360 - page["/Rotate"]
        page.rotate(x)
        pdf_writer.add_page(page)
    elif page["/Rotate"] == 0:
        pdf_writer.add_page(page)

output_path = Path.home() / "unscrambled.pdf"

with output_path.open(mode="wb") as output_file:
    pdf_writer.write(output_file)
