nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
preopsitions = ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensously"]

import random

adj1 = random.choice(adjectives)
noun1 = random.choice(adjectives)
verb1 = random.choice(verbs)
prep1 = random.choice(preopsitions)
adj2 = random.choice(adjectives)
noun2 = random.choice(adjectives)
verb2 = random.choice(verbs)
prep2 = random.choice(preopsitions)
adj3 = random.choice(adjectives)
noun3 = random.choice(adjectives)
verb3 = random.choice(verbs)
prep3 = random.choice(preopsitions)
adv1 = random.choice(adverbs)


if adj1[0] in "aeiou":
    a_an = "An"
else:
    a_an = "A"


print(f"{a_an} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}")
print(f"{adv1}, the {noun1} {verb2}")
print(f"the {noun2} {verb3} {prep2} a {adj3} {noun3}")
