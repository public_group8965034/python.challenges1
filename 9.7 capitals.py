capitals_dist = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
}

import random

random_state = random.choice(list(capitals_dist))
print(f"What is the capital of {random_state}?")
user_answer = ""

while user_answer.capitalize() != capitals_dist[random_state]:
    user_answer = input("Please write an answer. If You don't know, just write 'exit' ")

    if user_answer.capitalize() == capitals_dist[random_state]:
        print("Correct")
        break
    elif user_answer.lower() == "exit":
        print("I see that you can't do it")
        break
