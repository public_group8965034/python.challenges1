import tkinter as tk
import random
from tkinter import messagebox

parts_of_hangman = []


def hangman_draw(n):
    if n > 0:
        ground = hangman_canva.create_line(20, 180, 180, 180, width=3)
        parts_of_hangman.append(ground)
    if n > 1:
        beam = hangman_canva.create_line(100, 180, 100, 20, width=3)
        parts_of_hangman.append(beam)
    if n > 2:
        rope = hangman_canva.create_line(100, 20, 140, 20, width=3)
        parts_of_hangman.append(rope)
    if n > 3:
        head = hangman_canva.create_oval(130, 20, 160, 50, width=3)
        parts_of_hangman.append(head)
    if n > 4:
        body = hangman_canva.create_line(145, 50, 145, 100, width=3)
        parts_of_hangman.append(body)
    if n > 5:
        left_arm = hangman_canva.create_line(145, 60, 120, 80, width=3)
        parts_of_hangman.append(left_arm)
    if n > 6:
        right_arm = hangman_canva.create_line(145, 60, 170, 80, width=3)
        parts_of_hangman.append(right_arm)
    if n > 7:
        left_leg = hangman_canva.create_line(145, 100, 130, 130, width=3)
        parts_of_hangman.append(left_leg)
    if n > 8:
        right_leg = hangman_canva.create_line(145, 100, 160, 130, width=3)
        parts_of_hangman.append(right_leg)


def make_guess():
    global word
    global guess_word
    global guess_attempt

    guess = guess_entry.get().lower()

    if len(guess) == 1 and guess.isalpha():
        if guess in word:
            for i, letter in enumerate(word):
                if letter == guess:
                    guess_word[i] = guess
            word_label.config(text=" ".join(guess_word))
        else:
            hangman_draw(guess_attempt)
            guess_attempt += 1
    else:
        messagebox.showinfo("Invalid Input", "Please enter a single letter.")

    guess_entry.delete(0, tk.END)

    if "_" not in guess_word:
        messagebox.showinfo("Congratulations!", "You guessed the word!")
    elif guess_attempt >= 9:
        messagebox.showinfo("Game Over", f"You didn't guess the word. It was: {word}")


window = tk.Tk()
window.title("HANGMAN THE GAME")
window.geometry("600x600")

hangman_canva = tk.Canvas(window, width=200, height=200)
hangman_canva.pack()

words_list = ["python", "programowanie", "komputer", "gry", "wisielec"]
word = random.choice(words_list)
guess_word = ["_"] * len(word)

guess_attempt = 1

word_label = tk.Label(window, text=" ".join(guess_word), font=("Helvetica", 18))
word_label.pack(pady=20)

guess_entry = tk.Entry(window, width=5)
guess_entry.pack()

guess_button = tk.Button(window, text="Guess", command=make_guess)
guess_button.pack(pady=10)

window.mainloop()