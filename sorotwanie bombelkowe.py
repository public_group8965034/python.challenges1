lista = [1, 21, 20, 11, 7, 6, 3, -7, -12]
def bubble_sorting(list):
    list_range = len(list)
    while list_range > 0:
        for x in range(list_range-1):
           if list[x] > list[x+1]:
               list[x], list[x+1] = list[x+1],list[x]
        list_range -= 1
        #print(list)
    return list


print(bubble_sorting(lista))
