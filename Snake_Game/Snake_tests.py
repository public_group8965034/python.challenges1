import pygame
from sys import exit
import random

class Player(pygame.sprite.Sprite):
    global head_index
    def __init__(self):
        super().__init__()
        head_up = pygame.image.load("Graphics/head_up.png").convert_alpha()
        head_down = pygame.image.load("Graphics/head_down.png").convert_alpha()
        head_left = pygame.image.load("Graphics/head_left.png").convert_alpha()
        head_right = pygame.image.load("Graphics/head_right.png").convert_alpha()
        self.head_index = 1
        self.head = [head_up,head_right,head_down,head_left]
        self.head_x = 440
        self.head_y = 400
        self.step = 40
        self.image = self.head[self.head_index]
        self.rect = self.image.get_rect(topleft= (self.head_x, self.head_y))
        self.last_update_time = pygame.time.get_ticks()
        self.history = []

    def player_input(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT] and self.head_index != 1:
            self.head_index = 3
        if keys[pygame.K_RIGHT] and self.head_index != 3:
            self.head_index = 1
        if keys[pygame.K_UP] and self.head_index != 2:
            self.head_index = 0
        if keys[pygame.K_DOWN] and self.head_index != 0:
            self.head_index = 2

    def movement(self):
        current_time = pygame.time.get_ticks()

        if current_time - self.last_update_time >= 50:
            if self.head_index == 0:
                self.head_y -= self.step
            if self.head_index == 1:
                self.head_x += self.step
            if self.head_index == 2:
                self.head_y += self.step
            if self.head_index == 3:
                self.head_x -= self.step
            self.last_update_time = current_time
            self.history.append((self.head_x, self.head_y))

    def get_n_earlier_position(self, n):
        if len(self.history) > 400:
            self.history = self.history[-400:]
        if n < len(self.history):
            return self.history[-n]
        return None


        return None

    def animation_stet(self):
        self.image = self.head[self.head_index]
        self.rect = self.image.get_rect(topleft=(self.head_x, self.head_y))

    def get_head_position(self):
        return self.head_x, self.head_y


    def update(self):
        self.player_input()
        self.movement()
        self.animation_stet()

class Body(pygame.sprite.Sprite):

    def __init__(self, player_head, head_index, body_x, body_y, lenght ):
        super().__init__()
        self.player_head = player_head
        self.head_index = head_index
        body_bottomleft = pygame.image.load("Graphics/body_bottomleft.png").convert_alpha()
        body_bottomright = pygame.image.load("Graphics/body_bottomright.png").convert_alpha()
        body_topleft = pygame.image.load("Graphics/body_topleft.png").convert_alpha()
        body_topright = pygame.image.load("Graphics/body_topright.png").convert_alpha()
        body_horizontal = pygame.image.load("Graphics/body_horizontal.png").convert_alpha()
        body_vertical = pygame.image.load("Graphics/body_vertical.png").convert_alpha()
        self.body_index = 4
        self.body = [body_bottomleft, body_bottomright, body_topleft, body_topright, body_horizontal,body_vertical]
        self.body_x = body_x
        self.body_y = body_y
        self.image = self.body[self.body_index]
        self.rect = self.image.get_rect(topleft= (self.body_x, self.body_y))
        self.step = 40
        self.length = lenght


    def head_pos(self):
        head_x, head_y = self.player_head.get_head_position()
        return head_x, head_y


    def body_part(self):

        previous_x, previous_y= (body_part_group.sprites()[(self.length - 2)]).body_positon()
        next_x, next_y = (body_part_group.sprites()[(self.length -3)]).body_positon()
        body_pos = self.player_head.get_n_earlier_position(self.length)



        if self.length > 2:
            if body_pos is not None:
                self.body_x, self.body_y = body_pos


            if previous_x == next_x:
                self.body_index = 5
            elif previous_y == next_y:
                self.body_index = 4
            elif (previous_x + self.step) == next_x and (previous_y + self.step) == next_y and self.body_y == previous_y:
                self.body_index = 0
            elif (previous_x + self.step) == next_x and (previous_y + self.step) == next_y and self.body_x == previous_x:
                self.body_index = 3
            elif (previous_x - self.step) == next_x and (previous_y - self.step) == next_y and self.body_y == previous_y:
                self.body_index = 3
            elif (previous_x - self.step) == next_x and (previous_y - self.step) == next_y and self.body_x == previous_x:
                self.body_index = 0
            elif (previous_x - self.step) == next_x and (previous_y + self.step) == next_y and self.body_y == previous_y:
                self.body_index = 1
            elif (previous_x - self.step) == next_x and (previous_y + self.step) == next_y and self.body_x == previous_x:
                self.body_index = 2
            elif (previous_x + self.step) == next_x and (previous_y - self.step) == next_y and self.body_y == previous_y:
                self.body_index = 2
            elif (previous_x + self.step) == next_x and (previous_y - self.step) == next_y and self.body_x == previous_x:
                self.body_index = 1



        elif self.length == 2:

            if body_pos is not None:
                self.body_x, self.body_y = body_pos
            if head_index == 2:
                if self.body_x - self.step == self.head_pos()[0] and self.body_y == self.head_pos()[1]:
                    self.body_index = 2
                elif self.body_x + self.step == self.head_pos()[0] and self.body_y == self.head_pos()[1]:
                    self.body_index = 3
                else:
                    self.body_index = 5
            if head_index == 1:
                if self.body_x == self.head_pos()[0] and self.body_y - self.step == self.head_pos()[1]:
                    self.body_index = 2
                elif self.body_x == self.head_pos()[0] and self.body_y + self.step == self.head_pos()[1]:
                    self.body_index = 0
                else:
                    self.body_index = 4
            if head_index == 0:
                if self.body_x - self.step == self.head_pos()[0] and self.body_y == self.head_pos()[1]:
                    self.body_index = 0
                elif self.body_x + self.step == self.head_pos()[0] and self.body_y == self.head_pos()[1]:
                    self.body_index = 1
                else:
                    self.body_index = 5
            if head_index == 3:
                if self.body_x == self.head_pos()[0] and self.body_y - self.step == self.head_pos()[1]:
                    self.body_index = 3
                elif self.body_x  == self.head_pos()[0] and self.body_y + self.step == self.head_pos()[1]:
                    self.body_index = 1
                else:
                    self.body_index = 4

    def animiation_body(self):
        self.image = self.body[self.body_index]
        self.rect = self.image.get_rect(topleft=(self.body_x, self.body_y))

    def body_positon(self):
        return self.body_x, self.body_y

    def update(self):
        self.body_part()
        self.head_pos()
        self.animiation_body()

class Star(pygame.sprite.Sprite):

    def __init__(self, star_x, star_y):
        super().__init__()
        self.star = pygame.image.load("Graphics/star.png").convert_alpha()
        self.star_x = star_x
        self.star_y = star_y
        self.image = self.star
        self.rect = self.image.get_rect(topleft= (self.star_x, self.star_y))


    def animiation_star(self):
        self.image = self.star
        self.rect = self.image.get_rect(topleft= (self.star_x, self.star_y))

    def update(self):

        self.animiation_star()

class Score(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.score = 0
        self.font = pygame.font.Font(None, 80)
        self.color = (255, 173, 0)
        self.position = (970, 330)

    def update_score(self, increment):
        self.score += increment

    def draw(self, screen):
        score_text = self.font.render(str(self.score), True, self.color)
        screen.blit(score_text, self.position)

def star_pos_generation():
    matrix = []
    for x in range(0,761, 40):
        for y in range(0,761,40):
                matrix.append((x, y))
    star_x, star_y = random.choice(matrix)
    return star_x, star_y

class Tail(pygame.sprite.Sprite):
    def __init__(self, body, last_part_body_pos):
        super().__init__()
        tail_up = pygame.image.load("Graphics/tail_up.png").convert_alpha()
        tail_down = pygame.image.load("Graphics/tail_down.png").convert_alpha()
        tail_left = pygame.image.load("Graphics/tail_left.png").convert_alpha()
        tail_right = pygame.image.load("Graphics/tail_right.png").convert_alpha()
        self.tail_index = 3
        self.tail = [tail_up, tail_right, tail_down, tail_left]
        self.tail_x = 320
        self.tail_y = 400
        self.step = 40
        self.image = self.tail[self.tail_index]
        self.rect = self.image.get_rect(topleft=(self.tail_x, self.tail_y))
        self.body = body
        self.last_part_body_pos = last_part_body_pos

    def animation_tail(self):
        self.image = self.tail[self.tail_index]
        self.rect = self.image.get_rect(topleft=(self.tail_x, self.tail_y))

    def tail_pos(self):
        tail_x, tail_y = self.last_part_body_pos
        self.tail_x = tail_x
        self.tail_y = tail_y

    def update(self):
        self.tail_pos()
        self.animation_tail()


snake_position = []

pygame.init()
screen = pygame.display.set_mode((1200, 800))
pygame.display.set_caption("SNAKE za rączke")
clock = pygame.time.Clock()
game_active = True
bg_surf = pygame.image.load("Graphics/background.png").convert_alpha()

player = pygame.sprite.GroupSingle()
player.add(Player())

head_index = player.sprite.head_index

body_part_group = pygame.sprite.Group()
body_part_group.add(Body(player.sprite, head_index,440,400,2))
body_part_group.add(Body(player.sprite, head_index,400,400,3))

movement = pygame.USEREVENT+1
pygame.time.set_timer(movement, 150)

head_pos_list = []

star_x, star_y = star_pos_generation()
star = pygame.sprite.GroupSingle()
star.add(Star(star_x, star_y))

tail_group = pygame.sprite.Group()  # ogon
tail_group.add(Tail(body_part_group.sprites()[-1], (40,40)))  # ogon


lenght_n = 3
score = Score()
game_over_surface = pygame.image.load("Graphics/game_over.png").convert()
while True:

    for event in pygame.event.get():

        if event.type == pygame.QUIT:
            pygame.quit()
            exit()

        if game_active:
            if event.type == movement:
                snake_position.clear()
                snake_position.append(player.sprite.get_head_position())
                for body_part in body_part_group.sprites():
                    snake_position.append(body_part.body_positon())
                player.update()
                body_part_group.update()
                head_index = player.sprite.head_index
                star.update()
                screen.blit(bg_surf, (0, 0))
                body_part_group.draw(screen)
                star.draw(screen)
                player.draw(screen)
                score.draw(screen)
                tail_group.update()
                tail_pos = player.sprite.get_n_earlier_position(lenght_n)
                tail_group.draw(screen)

                collision_star = pygame.sprite.spritecollide(player.sprite, star, True)
                if collision_star:
                    score.update_score(1)
                    body_x, body_y = body_part_group.sprites()[-1].body_positon()
                    lenght_n += 1
                    body_part_group.add(Body(player.sprite, head_index,body_x, body_y, lenght_n))
                    tail_group.sprites()[0].kill()
                    tail_pos = player.sprite.get_n_earlier_position(lenght_n)
                    tail_group.add(Tail(body_part_group.sprites()[-1], tail_pos))

                collision_body = pygame.sprite.spritecollide(player.sprite, body_part_group, False)
                if collision_body:
                    game_active = False

                head_x, head_y = player.sprite.get_head_position()
                if head_x < 0 or head_x > 760 or head_y < 0 or head_y > 760:
                    game_active = False

        else:
            screen.blit(game_over_surface, (0, 0))
            score.draw(screen)




    pygame.display.update()
    clock.tick(60)



