import pygame

class Tail(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        tail_up = pygame.image.load("Graphics/tail_up.png").convert_alpha()
        tail_down = pygame.image.load("Graphics/tail_down.png").convert_alpha()
        tail_left = pygame.image.load("Graphics/tail_left.png").convert_alpha()
        tail_right = pygame.image.load("Graphics/tail_right.png").convert_alpha()
        self.tail_index = 3
        self.tail = [tail_up, tail_right, tail_down, tail_left]
        self.tail_x = 320
        self.tail_y = 400
        self.step = 40
        self.image = self.tail[self.tail_index]
        self.rect = self.image.get_rect(bottomleft= (self.tail_x, self.tail_y))


