import pygame

class Body(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        tail_up = pygame.image.load("Graphics/tail_up.png").convert_alpha()
        tail_down = pygame.image.load("Graphics/tail_down.png").convert_alpha()
        tail_left = pygame.image.load("Graphics/tail_left.png").convert_alpha()
        tail_right = pygame.image.load("Graphics/tail_right.png").convert_alpha()
        self.tail_index = 3
        self.tail = [tail_up, tail_right, tail_down, tail_left]
        self.tail_x = 320
        self.tail_y = 400
        self.step = 40
        self.image = self.tail[self.tail_index]
        self.rect = self.image.get_rect(bottomleft= (self.tail_x, self.tail_y))

        body_bottomleft = pygame.image.load("Graphics/body_bottomleft.png").convert_alpha()
        body_bottomright = pygame.image.load("Graphics/body_bottomright.png").convert_alpha()
        body_topleft = pygame.image.load("Graphics/body_topleft.png").convert_alpha()
        body_topright = pygame.image.load("Graphics/body_topright.png").convert_alpha()
        body_horizontal = pygame.image.load("Graphics/body_horizontal.png").convert_alpha()
        body_vertical = pygame.image.load("Graphics/body_vertical.png").convert_alpha()
        self.body_index = 3
        self.body = [body_bottomleft, body_bottomright, body_topleft, body_topright, body_horizontal,body_vertical]
        self.body_x = 320
        self.body_y = 400
        self.image = self.body[self.body_index]
        self.rect = self.image.get_rect(bottomleft= (self.body_x, self.body_y))

    def head_pos(self):
        print(player.get_head_position())



    def update(self):
        self.head_pos()