import easygui as gui
from PyPDF2 import PdfReader, PdfWriter

input_path = gui.fileopenbox(
    title="Select a Pdf to open",
    default="*.pdf",
)

if input_path is None:
    exit()

while True:
    start_page = gui.enterbox(
        msg="Please ad first page number",
        title="Starting Page"
    )

    if start_page is None:
        exit()

    try:
        start_page = int(start_page)
        if start_page > 0:
            break
        else:
            print("Please enter a valid page number greater than zero")
    except ValueError:
        print("Please enter a valid page number")

while True:
    end_page = gui.enterbox(
        msg="Please ad last page number",
        title="Last Page"
    )

    if end_page is None:
        exit()

    try:
        end_page = int(end_page)
        if end_page > start_page:
            break
        else:
            print("Please enter a valid page number greater than starting page")
    except ValueError:
        print("Please enter a valid page number")

save_title = "Save the rotated PDF as..."
file_type = "*.pdf"
output_path = gui.filesavebox(title=save_title, default=file_type)


while input_path == output_path:
    gui.msgbox(msg="Cannot overwrite orinal file!")
    output_file = gui.filesavebox(title=save_title, default=file_type)

if output_path is None:
    exit()

input_file = PdfReader(input_path)
output_pdf = PdfWriter()

for i, page in enumerate(input_file.pages):p[r]
    if start_page <= i + 1 <= end_page:
        output_pdf.add_page(page)

with open(output_path, "wb") as output_file:
    output_pdf.write(output_file)
