class Animal:
    def __init__(self, species, age):
        self.species = species
        self.age = age

    def __str__(self):
        return f"{self.species} is {self.age}"

    def speak(self, sound):
        return f"{self.species} says {sound}"

    def eats(self, food):
        return f"{self.species} loves to eat {food}"


class InBarn(Animal):
    def eats(self, food):
        return f"{self.species} in the barn loves to eat {food}"


class OutsideBarn(Animal):
    pass

kasztanka = InBarn("Horse", 3)
pepa = InBarn("Pig", 2)
milka = OutsideBarn("Cow", 3)
dolly = OutsideBarn("Sheep", 4)

print(pepa)
print(type(milka))
print(isinstance(dolly, InBarn))
print(kasztanka.eats("hay"))
