def convert_cel_to_far(c):
    """Converts celsius to fahrenheit"""
    temp_f = c * 9/5 + 32
    return temp_f

def convert_far_to_cel(f):
    """Converts fahrenheit to celsius"""
    temp_c = (f - 32) * 5/9
    return temp_c

c = float(input("Enter a temperature in C: "))

x = convert_cel_to_far(c)

print(f"{c} degrees C = {x:.2f} degrees F")

f = float(input("Enter a temperature in F: "))

y = convert_far_to_cel(f)

print(f"{f} degrees F = {y:.2f} degrees C")
